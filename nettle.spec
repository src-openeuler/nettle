Name:           nettle
Version:        3.10.1
Release:        2
Summary:        A low-level cryptographic library
License:        LGPL-3.0-or-later OR GPL-2.0-or-later
URL:            https://www.lysator.liu.se/~nisse/nettle/
Source0:        https://www.lysator.liu.se/~nisse/archive/%{name}-%{version}.tar.gz
Patch0:         nettle-3.10.1-hobble-to-configure.patch

BuildRequires: gcc make
BuildRequires: autoconf automake libtool
BuildRequires: gettext-devel
BuildRequires: gmp-devel >= 1:6.1.0

%description
Nettle is a cryptographic library designed to fit any context:
in crypto toolkits for object-oriented languages, in applications
like LSH or GnuPG, or even in kernel space.

%package devel
Summary:        Development headers for %{name} package
Requires:       %{name} = %{version}-%{release} gmp-devel

%description devel
The devel for %{name}

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -ifv
%configure --enable-shared --enable-fat --disable-static \
   --enable-sm3 --enable-sm4 \
   --disable-ecc-secp192r1 --disable-ecc-secp224r1
%make_build

%install
%make_install
%delete_la
make install-shared
install -D -p -m0644 nettle.info "$RPM_BUILD_ROOT%{_infodir}/nettle.info"

chmod 0755 $RPM_BUILD_ROOT%{_libdir}/libnettle.so.8.*
chmod 0755 $RPM_BUILD_ROOT%{_libdir}/libhogweed.so.6.*

rm -f %{buildroot}%{_bindir}/*

%check
%make_build check

%files
%doc AUTHORS descore.README nettle.pdf
%license COPYINGv2 COPYINGv3 COPYING.LESSERv3
%{_infodir}/nettle.info*
%{_libdir}/libnettle.so.8*
%{_libdir}/libhogweed.so.6*

%files devel
%{_includedir}/nettle/
%{_libdir}/libnettle.so
%{_libdir}/libhogweed.so
%{_libdir}/pkgconfig/*.pc

%files help
%doc NEWS README 
%doc nettle.html

%changelog
* Wed Feb 12 2025 Funda Wang <fundawang@yeah.net> - 3.10.1-2
- sync hobble patch with fedora
- drop unused fipscheck build dependency

* Wed Jan 22 2025 Funda Wang <fundawang@yeah.net> - 3.10.1-1
- update to 3.10.1

* Sun Nov 03 2024 Funda Wang <fundawang@yeah.net> - 3.10-1
- update to 3.10
- use patch disabling algorithms on request

* Mon Oct 31 2022 gaihuiying <eaglegai@163.com> - 3.8.1-1
- Type:requirement
- Id:NA
- SUG:NA
- DESC:update to release-3.8.1

* Wed Mar 23 2022 xingwei <xingwei14@h-partners.com> - 3.7.3-2
- Type:bugfix
- Id:NA
- SUG:restart
- DESC:delete useless so file

* Sat Dec 04 2021 yanglu <yanglu72@huawei.com> - 3.7.3-1
- Type:requirements
- Id:NA
- SUG:NA
- DESC:update nettle to 3.7.3

* Mon Aug 16 2021 gaihuiying <gaihuiying1@huawei.com> - 3.6-7
- Type:CVE
- CVE:CVE-2021-3580
- SUG:NA
- DESC:fix CVE-2021-3580
* Mon Apr 19 2021 xihaochen <xihaochen@huawei.com> - 3.6-6
- Type:CVE
- CVE:CVE-2021-20305
- SUG:NA
- DESC:fix CVE-2021-20305

* Tue Jul 21 2020 cuibaobao <cuibaobao1@huawei.com> - 3.6-5
- Type:update
- Id:NA
- SUG:NA
- DESC:update to 3.6 

* Mon Oct 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.4.1rc1-4
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:add COPYINGv3 in license

* Wed Sep 04 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.4.1rc1-3
- Package init
